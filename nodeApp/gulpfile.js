// imports

var gulp = require('gulp'),
    uncss = require('gulp-uncss'),
    minifycss = require('gulp-minify-css'),
    notify = require('gulp-notify');

// tasks
gulp.task('styles', function () {
    return gulp.src('public/css/bootstrap.min.css')
        .pipe(minifycss())
        .pipe(gulp.dest('public/css/production'))
        .pipe(notify({message: 'Task styles is complete.'}));
});

// default
gulp.task('default', ['styles'], function () {
    
});