// IMPORTS
var express 		= require('express')
  , cookieParser 	= require('cookie-parser')
  , session 		= require('express-session')
  , bodyParser 		= require('body-parser')
  , pass 			= require('pwd')
  , fs            	= require('fs')
  , mongoose      	= require('mongoose')

var app = express();

// APP SETTINGS
app.set('env', 'development');
app.set('view engine', 'html');
app.engine('html', require('hbs').__express);

// MIDDLEWARES

app.use(bodyParser());
app.use(express.static(__dirname + '/public'));
app.use(cookieParser())
app.use(session({ secret: '3jfv0(kasdjkA1)*hn_kkfH@ld+', key: 'sid'})) // ! Do not forget to set "cookie: { secure: true }}"

// ROUTES

require('./routes/index')(app); // root, login, logout
require('./routes/admin')(app); // admin part of site
require('./routes/pictures')(app); // routes for pictures and albums manipulation

// APP
// [place-holder]

// DB SERVER LAUNCH
mongoose.connect('mongodb://127.0.0.1:27017/picshare');
mongoose.connection.on('error', console.error.bind(console, 'connection error:'));

// WEB SERVER LAUNCH
var server = app.listen(3000, function() {
    console.log('Listening on port %d', server.address().port);
});