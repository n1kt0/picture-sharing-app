define([
	'underscore',
	'backbone',
	'models/user'
	], function(_, Backbone, user){
		var Users = Backbone.Collection.extend({
			model: user,
			url: '/users'
		})

		return new Users;
})