define([
	'underscore',
	'backbone',
	'models/album'
	], function(_, Backbone, album){
		var Albums = Backbone.Collection.extend({
			model: album,
			url: '/albums'
		})

		return new Albums;
})