define(['underscore', 'backbone'], function(_, Backbone){
		var UserModel = Backbone.Model.extend({
			idAttribute: '_id',
			defaults: {
				name: 'Test',
				role: 'user'
			}
		});

		return UserModel;
	});