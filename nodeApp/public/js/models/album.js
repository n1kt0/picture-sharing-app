define(['underscore', 'backbone'], function(_, Backbone){

	var AlbumModel = Backbone.Model.extend({
		idAttribute: '_id',
		defaults: {
			title: 'New Album',
			desc: '',
			title_image: '',
			owner: '',
			shared: false,
			shared_with: [],
			pictures: []
		}
	});

	return AlbumModel;
});