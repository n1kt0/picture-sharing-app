define(['underscore', 'backbone'], function(_, Backbone){
	var PictureModel = Backbone.Model.extend({
		idAttribute: '_id',
		urlRoot: '/pictures',
		defaults: {
			title: '',
			path: '',
			thumb_path: '',
			album_title: false,
			album: [],
		}
	});

	return PictureModel;
})