var app = app || {};

var router = Backbone.Router.extend({
	routes: {
		'users'		: 	'showUsers',
		'general'	: 	'showGeneral',
		'*other'	: 	'showDefault'
	},

	AppViews: {
		MainView: '',
		SideView: '',
		ModalView: ''
	},

	loadView: function(viewType, view){
		// if the same type view is already loaded - remove it (either with its .close() function or explicitly with .remove() method).
		var currentView = this.AppViews[viewType];
		if (currentView) {
			if (currentView.close) {
				currentView.close()
			} else {
				currentView.remove()
			};
		};
		// cache the view being loaded to be able to delete it with next loadView() call.
		this.AppViews[viewType] = view;
	},

	showUsers: function(){
		this.loadView('MainView', new app.UsersView());
		!this.AppViews['ModalView'] && this.loadView('ModalView', new app.AddUserModalView())
	},

	showGeneral: function(){

	},

	showDefault: function(){
		this.loadView('MainView', new app.UsersView());
		this.loadView('ModalView', new app.AddUserModalView())
	}

});

app.AdminRouter = new router();
Backbone.history.start()