define([
	'jquery',
	'underscore',
	'backbone',
	'collections/albums',
	'views/app/albums',
	'views/app/album',
	'views/app/sideNav',
	'views/modal/addAlbum',
	'views/modal/shareAlbum',
	'views/modal/addPictures'
	], function($, _, Backbone, Albums, AlbumsView, AlbumView, SideNavView, AddAlbumModalView, ShareAlbumModalView, AddPicturesModalView){

	// Creating main router of the app
	var AppRouter = Backbone.Router.extend({
		routes:{
			"albums/:id"	: 	"showAlbum",
			"*other"		: 	"defaultRoute"
		},
		
		// All app views will be cached here for their loading control
		AppViews: {
			MainView: '',
			SideView: '',
			ModalView: ''
		},

		// Will keep track of albumID
		albumFilter: '',

		/*
		 * loadView() below takes care of insertion/deletion of view in a proper way to eliminate memory leaks.
		 * It takes 2 arguments: 
		 *   viewType - can be one of following: MainView, SideView, TopView. They correspond to the section of the page.
		 *	 	   So, MainView is the main app view (albums list or pics list on an album page or admin page). SideView
		 *	 	   is the view of a sidebar. And ModalView is the modal windows section view.
		 *	 view 	 - any Backbone view;
		 */
		loadView: function(viewType, view){
			// if there is same type view loaded - remove it either with its .close() function or explicitly with .remove() method.
			if (this.AppViews[viewType]) {
				if (this.AppViews[viewType].close) {
					this.AppViews[viewType].close();
				} else {
					this.AppViews[viewType].remove();
				};
			};

			// cache the view being loaded to be able to delete it with next loadView() call.
			this.AppViews[viewType] = view;
		},
		
		defaultRoute: function(other) {
			this.albumFilter = ''; // clearing the albumFilter since this is a root page
			this.loadView('MainView', new AlbumsView());
			this.loadView('SideView', new SideNavView({attributes: {albumFilter: this.albumFilter, userID: this.userID}})); // usderID is stored into router at time of its instantination when page is returned by server. See index.html.
			this.loadView('ModalView', new AddAlbumModalView());
		},

		showAlbum: function(id){
			this.albumFilter = id;
			this.loadView('SideView', new SideNavView({attributes: {albumFilter: this.albumFilter, userID: this.userID}}));
			this.loadView('MainView', new AlbumView({model: Albums.get(id), attributes: {albumFilter: this.albumFilter}})); 	// get and pass the model using ID from URL

			// Since there are 2 modal views on Album page, listen to the Album view and load required modal upon request
			this.listenTo(this.AppViews.MainView, 'loadModal', function(modalID){
				var ModalView;

				if (modalID == 'add-pictures') {
					ModalView = new AddPicturesModalView({attributes: {albumFilter: this.albumFilter}});
				} else if (modalID == 'share-album') {
					ModalView = new ShareAlbumModalView({attributes: {albumFilter: this.albumFilter}});
				} else {
					console.log('No valid modal-window type could be defined.')
					return;
				}
				this.loadView('ModalView', ModalView);
			});
		}
	});
	
	return AppRouter;
});