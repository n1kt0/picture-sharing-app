define([
	'jquery',
	'underscore',
	'backbone',
	'text!templates/side-bar-item.html'
	], function($, _, Backbone, sideNavItemTemplate){

	var SideNavItemView = Backbone.View.extend({
		tagName: 'li',
		
		template: _.template(sideNavItemTemplate),

		initialize: function(){
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'destroy', this.remove);
		},

		render: function(){
			this.$el.html(this.template(this.model.toJSON()));
			if (this.model.get('_id') == this.attributes.albumFilter){		// highlights the album as active if it is currently viewed
				this.$el.addClass('active');
			}
			return this;	
		}
	});

	return SideNavItemView;
})