define([
	'jquery',
	'underscore',
	'backbone',
	'collections/albums',
	'views/app/albumItem',
	'text!templates/album-list.html'
	], function($, _, Backbone, Albums, AlbumItemView, albumListTemplate){

	var AlbumsView = Backbone.View.extend({
		tagName: 'div',

		template: _.template(albumListTemplate),

		events: {
			'click #add-album': 'showaddAlbumForm'
		},

		initialize: function() {
			this.listenTo(Albums, 'add', this.addAlbum);
			this.listenTo(Albums, 'reset', this.addAlbums);
			
			this.sub_views = [];
			this.render();
		},
		
		render: function(){
			$('#main-div').append(this.$el.html(this.template()));
			this.addAlbums();
			return this;
		},

		addAlbums: function(){
			if (Albums.length) {
				this.$('#album-list').html('');
				Albums.each(this.addAlbum, this);	
			}
		},

		addAlbum: function(album){
			if (Albums.length ==1 ) {
				this.$('#album-list p:last').remove();
			}
			var view = new AlbumItemView({model:album});
			this.$('#album-list').append(view.render().el);

			// Adding sing album view to the list of sub_views of the Albums list view (main view)
			this.sub_views.push(view);
		},

		showaddAlbumForm: function(){
			$("div.modal").show();
		},

		close: function(){ // deletes this view and all sub-view to eliminate memory leaks
			_.each(this.sub_views, function(sub_view){
				sub_view.remove();
			});

			this.remove();
		}
	});

	return AlbumsView;
})