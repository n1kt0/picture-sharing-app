define([
	'jquery',
	'underscore',
	'backbone',
	'collections/albums',
	'views/app/sideNavItem',
	'text!templates/side-bar.html'
	], function($, _, Backbone, Albums, SideNavItemView, sideNavTemplate){

	var SideNavView = Backbone.View.extend({
		tagName: 'div',

		template: _.template(sideNavTemplate),

		initialize: function(){
			this.listenTo(Albums, 'add', this.addItem);
			this.listenTo(Albums, 'reset', this.addItems);
			this.render();
		},

		render: function(){
			$('#nav-sidebar').append(this.$el.html(this.template()));
			this.addItems();
			return this;
		},

		addItem: function(album){
			var view 		= new SideNavItemView({model: album, attributes: {albumFilter: this.attributes.albumFilter}}),
				albumOwner 	= album.get('owner');

			// Now append view either to own albums list or to the shared ones. If it is newly created album, add it to own albums by default
			(this.attributes.userID == albumOwner) || !albumOwner ? this.$('#my-albums').append(view.render().el) :
									   this.$('#shared-albums').append(view.render().el)

		},

		addItems: function(){
			this.$('#my-albums').html('');
			Albums.each(this.addItem, this);
		}
	});

	return SideNavView;
})