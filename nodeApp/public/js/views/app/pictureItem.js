define([
	'jquery',
	'underscore',
	'backbone',
	'collections/albums',
	'text!templates/picture-item.html'
	], function($, _, Backbone, Albums, pictureTemplate){
	var PictureItemView = Backbone.View.extend({
		tagName: 'div',
		className: 'col-xs-6 col-sm-3',

		template: _.template(pictureTemplate),

		events:{
			'click .glyphicon-star': 'makeItAlbumTitle',
			'click .glyphicon-remove': 'erasePicture'
		},

		initialize: function(){
			this.listenTo(this.model, 'destroy', this.remove);
			
			this.albumID = this.model.get('album')[0];
			
			this.render();
		},

		render: function(){
			this.$el.html(this.template(this.model.toJSON()));
			return this;
		},

		makeItAlbumTitle: function(){
			Albums.get(this.albumID).save({title_image: this.model.get('thumb_path')});
		},

		erasePicture: function(){
			this.model.destroy();

			// Now need to update Album view and its model: delete this picture from it
			var album 	= Albums.get(this.albumID),
				pics 	= album.get('pictures'), 		// get the array of all pics in the corresponding album
				self 	= this,					 		// cache the context for usage in below filter() func
				updated = pics.filter(function(pic) { 	// prepare new pics array without this pic
							return pic._id !== self.model.get('_id');
						});
			album.set('pictures', updated); 	 		// update the model locally (do not send any req to server)
		}
	})

	return PictureItemView;
})