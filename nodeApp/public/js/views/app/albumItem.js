define([
	'jquery',
	'underscore',
	'backbone',
	'text!templates/album-item.html'
	], function($, _, Backbone, albumTemplate){
	
	var AlbumItemView = Backbone.View.extend({
		tagName: 'div',
		className: 'col-xs-6 col-sm-3',

		events:{
			'click .glyphicon-remove': 'removeAlbum'
		},
		
		template: _.template(albumTemplate),
		
		initialize: function(){
			this.listenTo(this.model, 'change', this.render);
			this.listenTo(this.model, 'destroy', this.remove);
		},

		render: function(){
			var data = this.model.toJSON(); 												// caching the model for template
			var title_image = _.findWhere(this.model.get('pictures'), {album_title: true});	// checking if there is a title image for album
			if (title_image) {
				data.title_image = title_image.thumb_path;							// if there is one - add it to the data object for template
				}							
			this.$el.html(this.template(data));
			return this;
		},

		removeAlbum: function(){
			this.model.destroy();
		}
	});

	return AlbumItemView;
})