define([
	'jquery',
	'underscore',
	'backbone',
	'lightbox',
	'models/picture',
	'views/app/pictureItem',
	'views/modal/addPictures',
	'views/modal/shareAlbum',
	'text!templates/album-page.html'
	], function($, _, Backbone, lightbox, Picture, PictureItemView, AddPicturesModalView, ShareAlbumModalView, albumTemplate){

	var AlbumView = Backbone.View.extend({
		tagName: 'div',

		template: _.template(albumTemplate),

		events: {
			'click #add-pictures': 'showModal',
			'click #delete-album': 'deleteAlbum',
			'click #share-album': 'showModal',
			'dblclick h1' : 'editTitle',
			'blur input': 'saveTitle',
			'keypress input': 'createOnEnter', 
		},

		initialize: function(){
			this.listenTo(this.model, 'sync', this.updateAlbum);
			this.sub_views = [];	// if this view gets removed, be able to delete all sub-views
			this.pictures_total = this.model.get('pictures').length // number of pictures inside the album at time of initiation
			this.render();
		},

		render: function(){
			$('#main-div').append(this.$el.html(this.template(this.model.toJSON())));
			this.addPictures();
			return this;
		},

		updateAlbum: function(){
			var album_pictures 	= this.model.get('pictures'),					// getting current album pictures array
				picturesAdded 	= album_pictures.length - this.pictures_total;	// checking if there were any pictures added
			if (picturesAdded) {												// if added, then
				this.pictures_total = album_pictures.length;						// update the album pictures_total property
				var picturesToAdd = _.last(album_pictures, picturesAdded);			// get the array of added pictures
				_.each(picturesToAdd, this.addPicture, this)						// add new pictures to current view


			} else {															// otherwise update the entire view
				this.$el.html(this.template(this.model.toJSON()));
				this.addPictures();
			}
		},

		addPicture: function(pictureData){
			this.$('#pictures-list').find('p').remove();
			var pictureModel = new Picture(pictureData);
			var view = new PictureItemView({model: pictureModel});
			this.$('#pictures-list').append(view.el)
			this.sub_views.push(view);
		},

		addPictures: function(){
			if (this.model.get('pictures').length) {
				this.$('#pictures-list').html('');
				var pictures = this.model.get('pictures');
				_.each(pictures, this.addPicture, this);	
			}
		},
		// [TODO]
		showModal: function(e){
			modalID = e.target.getAttribute('id');	// comes from button's id attribute. Can be either 'add-pictures' or 'share-album'
			
			this.trigger('loadModal', modalID); // trigger event which is listened by Router and passing it view we need to load to DOM.

			// app.AppRouter.loadView('ModalView', modalView);
			$("div.modal").show();
		},
		// [/TODO]
		editTitle: function(){							// enabling edit mode
			this.$('h1').addClass('hidden');
			this.$('input').removeClass('hidden').focus();
		},

		saveTitle: function(){
			this.$('h1').removeClass('hidden');
			this.$('input').addClass('hidden');
			var oldTitle = this.$('h1').text().trim();	// cache current title value
			var newTitle = this.$('input').val().trim();
			if (newTitle && newTitle !== oldTitle) { 	// check if title is not empty and is not the same
				this.model.save({title: newTitle}); 	// updating title in the DB
				this.$('h1').text(newTitle);			// updating title on the page
			} else {
				this.$('input').val(oldTitle)			// if the input value was not correct - change it back to old one
			}
		},

		createOnEnter: function(e){
			e.which === 13 && this.saveTitle();
		},

		deleteAlbum: function(){
			this.model.destroy() 							// deleting album model 
			this.close()									// removing this veiw and all sub-views
			app.AppRouter.navigate('#', {trigger: true})	// navigating back to the main page
		},

		close: function(){ // deletes this view and all sub-view to eliminate memory leaks
			_.each(this.sub_views, function(sub_view){
				sub_view.remove();
			});

			this.remove();
		}
	});

	return AlbumView;
})