define([
	'jquery',
	'underscore',
	'backbone',
	'collections/albums',
	'text!templates/modals/add-pictures.html'
	], function($, _, Backbone, Albums, addPicturesTemplate){
		
	var AddPicturesModalView = Backbone.View.extend({
		tagName: 'div',
		className: 'modal add-pictures',

		template: _.template(addPicturesTemplate),

		events: {
			'click button[name=submit]' : 'upload',
			'click .close': 'hide'
		},

		initialize: function(){
			this.render();
		},

		render: function(){ 	
			$('#modal-window').append(this.$el.html(this.template()));
			return this;
		},

		upload: function(){
			var files = this.$(':file')[0].files;
			var formData = new FormData();
			var albumID = this.attributes.albumFilter; // album id is got from Router.
			
			formData.append('aid', albumID);

			for (var i = 0; i < files.length; i++){
				var file = files[i];
				if (!file.type.match('image.*')) continue;
				
				formData.append('photos[]', file, file.name);
			};
			
			$.ajax({
				type: 'POST',
				url: '/albums/upload',
				data: formData,
				processData: false,
				contentType: false,
				success: function(res, text, jqXHR){
					Albums.get(albumID).fetch();
					$('#alert').
						text('Pictures successfully uploaded!').
						fadeIn(3000, function(){
								$('#alert').fadeOut(3000);
						});
				}
			})
			this.$(':file').val('');
			this.hide();
		},

		hide: function(){
			this.$el.hide();
		}
	});

	return AddPicturesModalView;
})