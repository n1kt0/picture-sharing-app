define([
	'jquery',
	'underscore',
	'backbone',
	'collections/albums',
	'text!templates/modals/add-album.html'
	], function($, _, Backbone, Albums, addAlbumTemplate){

	var AddAlbumModalView = Backbone.View.extend({
		tagName: 'div',
		className: 'modal add-album',

		template: _.template(addAlbumTemplate),

		events: {
			'click button[name=submit]' : 'addAlbum',
			'keypress input' : 'createOnEnter',
			'click .close': 'hide'
		},

		initialize: function(){
			this.render();
		},

		render: function(){ 	
			$('#modal-window').append(this.$el.html(this.template()));
			return this;
		},

		getAttributes: function(){
			return {
				_id: null,
				title: this.$('input[name=title]').val().trim(),
				desc: this.$('input[name=description]').val().trim(),
				title_image: ''
			}
		},

		addAlbum: function(e){
			var data = this.getAttributes();
			data.title ? 
				Albums.create(data) :
				console.log('Missting title');
			this.$('input[name=title]').val('');
			this.$('input[name=description]').val('');
			
			$('#alert').
						text('Album: "' + data.title + '" was created successfully!').
						fadeIn(3000, function(){
								$('#alert').fadeOut(3000);
						});
			
			this.hide();
		},

		createOnEnter: function(e){
			e.which === 13 && this.addAlbum();
		},

		hide: function(){
			this.$el.hide();
		}
	});

	return AddAlbumModalView;
})