define([
	'jquery',
	'underscore',
	'backbone',
	'collections/users',
	'collections/albums',
	'text!templates/modals/share-album.html'
	], function($, _, Backbone, Users, Albums, shareAlbumTemplate){
	
	var ShareAlbumModalView = Backbone.View.extend({
		tagName: 'div',
		className: 'modal share-album',

		template: _.template(shareAlbumTemplate),

		events: {
			'click button[name=submit]' : 'share',
			'click .close'				: 'hide',
		},

		initialize: function(){
			this.render();
		},

		render: function(){
			$('#modal-window').append(this.$el.html(this.template()));
		
			// Let's get list of users from server and append them to option list of the modal form
			Users.fetch({success: function(users){
				users.forEach(function(user){
					this.$('select[name=users]').append('<option value="' + user.get('_id') + '">' + user.get('name') + '</option>');
				}, this);
			}});
					
			return this;
		},

		share: function(){
			var selectedUsers 	= this.$('option:selected'),
				album 			= Albums.get(this.attributes.albumFilter),
				users 			= [];

			for (var i = 0, end = selectedUsers.length; i < end; i++){
				users.push(selectedUsers[i].value)
			};

			album.save({shared: users.length ? true : false, 
						shared_with: users});

			$('#alert').
						text('Album sharing was successfully updated!').
						fadeIn(3000, function(){
							$('#alert').fadeOut(3000);
						});

			this.hide();
		},

		hide: function(){
			this.$el.hide();
		}
	});

	return ShareAlbumModalView;
})