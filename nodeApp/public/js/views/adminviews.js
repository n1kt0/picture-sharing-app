var app = app || {}

// APP VIEWS

// User management page

app.UsersView = Backbone.View.extend({
	tagName: 'div',
	template: _.template($('#users-template').html()),

	events: {
		'click #add-user': 'showModal'
	},

	initialize: function(){
		this.listenTo(app.Users, 'add', this.appendUserData);
		this.listenTo(app.Users, 'reset', this.updateUserData);

		this.render();
		app.Users.fetch();
	},

	render: function(){
		$('#main-div').append(this.$el.html(this.template()));
		return this;
	},

	updateUserData: function(){
		app.Users.each(this.appendUserData, this);
	},

	appendUserData: function(user){
		var view = new app.TableItemView({model: user});
		this.$('tbody').append(view.render().el);	

	},

	showModal: function(){
		$("div.modal").show();
	}
});

app.TableItemView = Backbone.View.extend({ // user data table row view
	tagName: 'tr',
	template: _.template($('#user-data-template').html()),
	
	events: {
		'click #delete-user': 'deleteUser',
		'dblclick span': 'showEdit',
		'blur input': 'updateUser',
		'blur select': 'updateUser'
	},

	initialize: function(){
		this.listenTo(this.model, 'change', this.render);
		this.listenTo(this.model, 'destroy', this.remove);
	},

	render: function(){
		this.$el.html( this.template( this.model.toJSON() ) );
		return this;
	},

	deleteUser: function(){
		var userID = this.model.get('_id');
		$.ajax({
			type: 		'delete',
			url: 		'/users/' + userID,
			context: this,
			success: 	function(res, text, jqXHR){
				console.log(text)
				this.remove();
			}
		});
	},

	showEdit: function(e){
		var currentEl = e.target;
		var editEl = currentEl.nextElementSibling;

		currentEl.className += 'hidden'; // hiding current text
		editEl.className = editEl.className.replace('hidden', '').trim(); // showing input field;
		editEl.focus();
	},

	updateUser: function(e){
		var newValue = e.currentTarget.value.trim();
			var oldValue = e.currentTarget.previousElementSibling.textContent.trim();
		var field = e.currentTarget.name.replace('user-', ''); // provides the name of user attribute to update (either "name" or "role")
		
		// update model only if new and valid value was provided, otherwise ignore
		if (newValue && (newValue != oldValue)) this.model.set(field, newValue).save();

		e.currentTarget.className = 'hidden'; // hiding back the input field
		e.currentTarget.previousElementSibling.className = ''; // showing back the table text
	}


})

// MODAL WINDOW VIEWS

// Add user modal window 

app.AddUserModalView = Backbone.View.extend({
	tagName: 'div',
	className: 'modal add-user',
	template: _.template($('#add-user-modal').html()),

	events: {
		'click button[name=submit]': 'addUser',
		'click .close': 'hide'
	},

	initialize: function(){
		this.render()
	},

	render: function(){
		$('#modal-window').append(this.$el.html(this.template()));
		return this;
	},

	getAttributes: function(){
		var userName = this.$('input[name = username]').val().trim(),
			role	 = this.$('select[name = role]').val().trim(),
			pwd		 = this.$('input[name = password]').val().trim()

		if (userName && role && pwd) {
			return {
				username: userName,
				role: role,
				password: pwd
			}
		} else return;

	},

	addUser: function(){
		var userData = this.getAttributes();
		userData && $.ajax({ // if we have all required data make an ajax call
			type: 'post',
			url: '/users',
			data: userData,
			context: this,
			success: function(res, text, jqXHR){
				console.log(text + ' adding new user.');
				// Let's add new user data from response to app.Users colelction and trigger corresponding event, updating the view
				app.Users.add(res);
				// Hide the modal window
				this.hide();
				// Empty form:
				this.$('input[name = username]').val('');
				this.$('select[name = role]').val('user');
				this.$('input[name = password]').val('');
			}
		});
	},

	hide: function(){
		this.$el.hide();
	}
});