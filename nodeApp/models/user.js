// Imports
var mongoose = require('mongoose');

module.exports = {
	getUser: function(userData, callback) { // userData parameter shoud contain at least one of user fields: '_id' or'name'.
		var user = require('./models').User;
		user.findOne(userData, '_id name role', function(err, user){
			if (err) return callback(err, null);
			return callback(null, user.toJSON());
		});
		
	},

	getUserDeep: function(userData, callback) { // same as above, but gets all user data incl salt and hash
		var user = require('./models').User;
		user.findOne(userData, function(err, user){
			if (err) return callback(err, null);
			return callback(null, user.toJSON());
		});
		
	},

	getUsers: function(callback){
		var user = require('./models').User;
		user.find({}, 'name role _id', function(err, users){
			if (err) return callback(err, null);		
			return callback(null, users);
		});
	},
	deleteUser: function(userid, callback){
		var user = require('./models').User;
		user.findOneAndRemove({_id: userid}, function(){
			return callback();
		});	
	},
	authenticate: function(username, password, callback) {
		var hash = require('pwd').hash
		var getUser = require('./user').getUserDeep
		
		getUser({name: username}, function(err, user){
			if (user) {
				hash(password, user.salt, function(err, hash){
					if (err) return callback(err);
					if (hash == user.hash) {
						return callback(null, user);
					}
					callback(new Error('Invalid password'))
				});
			} else {
				return callback(new Error('Such username doesn\'t exist'))
			};
		});
	},
	addUser: function(username, role, password, callback){
		var hash = require('pwd').hash;
		hash(password, function(err, salt, hash){
			if (err) return callback(err);
			var user = require('./models').User;
			var newUser = new user({
				name: username,
				role: role,
				salt:salt,
				hash:hash
			})
			newUser.save(function(err, newUser){
				if (err) return callback(err);
				console.log('Added user ' + username);
				var newUserData = { // cutting off salt and hash data from the newUserobject we provide to callback
					_id: newUser._id,
					name: newUser.name,
					role: newUser.role
				}
				return callback(null,newUserData);
			});
		});
	},
	updateUser: function(userid, newData, callback){
		var user = require('./models').User;
		user.update({_id: userid}, newData, function(err){
			if (err) return callback(err);
			console.log('User with ID: ' + userid + ' was updated.');
			return callback();
		})
	}
}