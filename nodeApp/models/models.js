// Imports
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// App Schemes
var userSchema = new Schema({
	name: String,
	role: String,
	hash: String,
	salt: String
});

var albumSchema = new Schema({
	title: String,
	desc: String,
	title_image: String,
	owner: String,
	shared: Boolean,
	shared_with: [{type: Schema.Types.ObjectId, ref: 'User'}],
	pictures: [{type: Schema.Types.ObjectId, ref: 'Picture'}]
})

// albumSchema.methods.findSimilar = function(callback) {
// 	return this.model('Album').find({owner: this.owner}, callback);
// }

var pictureSchema = new Schema({
	title: String,
	path: String,
	thumb_path: String,
	album: [{type: Schema.Types.ObjectId, ref: 'Album'}],
	owner: [{type: Schema.Types.ObjectId, ref: 'User'}]

})

module.exports = {
	// Finally registering models which will be exported
	User: mongoose.model('User', userSchema),
	Album: mongoose.model('Album', albumSchema),
	Picture: mongoose.model('Picture', pictureSchema)
}