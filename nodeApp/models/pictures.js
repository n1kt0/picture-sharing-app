module.exports = {
	getAlbum : function(albumID, callback){
		var albums = require('./models').Album;
		albums
			.findOne({_id: albumID})
			.populate({	// No JOIN in mongoDB, so using this .populate method to get all pics relating to this album
				path: 'pictures',
				select: 'title path thumb_path album_title album'
				}) 
			.exec(function(err,album){
				if (err) return callback(err)
				// album.findSimilar(function(err, albums){ // getting the list of all albums with the same owner as current
				// 	return callback(null, {album: album, similar: albums}) // returning both current album and similar ones
				return callback(null, album);
			});
	},	
	getAlbums : function(userID, callback){
		var albums = require('./models').Album;

		albums.find( { $or: [{owner: userID}, {shared_with: userID}] }) // getting all albums both owned by user and shared with him
				.populate({
				  	path: 'pictures',
					select: 'title path thumb_path album_title album'
			   	})
			 	.exec(function(err, albums){
					if (err) return callback(err);
					return callback(null, albums);
			   	});
	},
	addAlbum: function(userID, title, titleImage, description, callback){
		var album = require('./models').Album;
		var newAlbum = new album({
			title: title,
			title_image: titleImage,
			desc: description,
			owner: userID,
			shared: false,
			shared_with: [],
			pictures: []
		});
		newAlbum.save(function(err, newAlbum){
			if (err) return callback(err);
			return callback(null, newAlbum.toJSON())
		});
	},
	deleteAlbum: function(albumID, callback){
		var albums 		= require('./models').Album,
			pictures 	= require('./models').Picture,
			fs			= require('fs'),
			path 		= require('path')

		albums.findOneAndRemove({_id: albumID}, function(err, album){
			if(album.pictures.length){		// delete related pictures too
				for (var i = 0; i < album.pictures.length; i++){
					pictures.findOneAndRemove({_id: album.pictures[i]}, function(err, picture){ // delete picture from DB
						if (err) return callback(err);
						fs.unlinkSync(path.normalize(__dirname + "/../public" + picture.path))  // and delete physically both file
						fs.unlinkSync(path.normalize(__dirname + "/../public" + picture.thumb_path)) // and thumb
					});
				}
			} else {						// no picture - just callback
				return callback();
			}			
		});
	},
	addPicture: function(pictureData, callback){
		var picture = require('./models').Picture;
		var newPicture = new picture({
			title: 			pictureData.title,
			path: 			pictureData.picturePath,
			thumb_path: 	pictureData.thumbPath,
			album_title: 	false,
			owner: 			pictureData.userID,
			album: 			pictureData.albumID
		});
		newPicture.save(function(err, newPicture){
			if (err) return callback(err);
			var albums = require('./models').Album
			albums.findOne({_id: pictureData.albumID}, function(err, album){	// populating the corresponding album with picture
				if (err) return callback(err);
				album.pictures.push(newPicture);
				album.save();
			})
			return callback(null, newPicture)
		});
	},
	shareAlbum: function(albumID, users, callback){
		var album 		= require('./models').Album,
			isShared 	= true;
		
		if (users == undefined) {
			var users = [];
			isShared =  false;
		}
		album.findOne({_id: albumID}, 'shared shared_with', function(err, album){
			album.update({shared: isShared, shared_with: users}, function(err){
				if (err) return callback(err);
				return callback();
			});
		});
	},
	updateAlbum: function(albumID, newData, callback){
		var album = require('./models').Album;
		album.update({_id: albumID}, newData, function(err){
			if (err) return callback(err);
			return callback();
		});
	},

	deletePicture: function(pictureID, callback){
		var pictures 	= require('./models').Picture,
			album 		= require('./models').Album,
			fs			= require('fs'),
			path 		= require('path')

		pictures.findOneAndRemove({_id: pictureID}, function(err, picture){
			if (err) return callback(err);
			fs.unlink(path.normalize(__dirname + "/../public" + picture.path));  		// delete physically both image
			fs.unlink(path.normalize(__dirname + "/../public" + picture.thumb_path)); 	// and its thumb
			
			// Corresponding album needs to be updated too
			var albumID = picture.album;
			
			album.findById(albumID, 'pictures', function(err, album){
				album.pictures.remove(picture._id);
				album.save();
			})

			return callback();
		});
	}
}