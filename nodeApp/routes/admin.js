// ROUTES MIDDLEWARES
var restrict = require('./middlewares').restrict
	,isAdmin = require('./middlewares').isAdmin

module.exports = function(app) {
	app.get('/admin', restrict, isAdmin, function(req, res){
		res.locals = {
			userName: req.session.user.name
		}

		res.render('admin');
	});
	app.get('/users', restrict, function(req, res){
		var getUsers = require('../models/user').getUsers;
		getUsers(function(err, users){
			// Checking if there were errors
			if (err) res.send('There was error:' + err);
			// No errors were returned, then we can send a page

			res.json(users)
		});
	});
	app.get('/users/:id', restrict, function(req, res){
		var userID = req.params.id;
		var getUser = require('../models/user').getUser;
		getUser({_id: userID}, function(err, user){
			if (err) res.send('There was an error: ' + err);
			res.json(user)
		});
	});
	app.post('/users', restrict, isAdmin, function(req, res){
		if (req.body.username.length == 0 || req.body.password.length == 0) {
			res.send('Empty forms are not processed.')
			return
		}
		var addUser = require('../models/user').addUser
		addUser(req.body.username, 
				req.body.role, 
				req.body.password, function(err, user){
					if (err) res.send('There was error:' + err);
					res.json(user);
		})
	});

	app.put('/users/:id', restrict, isAdmin, function(req, res){
		var userID = req.params.id;
		if (req.body.name.length == 0 || req.body.role.length == 0) {
			res.send('No valid data provided.')
			return
		}
		var newData = {
			name: req.body.name,
			role: req.body.role
		};
		var updateUser = require('../models/user').updateUser;
		updateUser(userID, newData, function(err){
			if (err) res.send('There was error:' + err);
		});
	});

	app.delete('/users/:id', restrict, isAdmin, function(req, res){
		var userID = req.params.id;
		var deleteUser = require('../models/user').deleteUser;
		deleteUser(userID, function(err){
			console.log('deleted user with ID: ' + userID);
			if (err) res.send(err);
			res.send('success!')
		});
	});
}