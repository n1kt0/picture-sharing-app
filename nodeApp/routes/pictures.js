// ROUTES MIDDLEWARES
var restrict = require('./middlewares').restrict,
	isOwner = require('./middlewares').isOwner


// ROUTES
module.exports = function(app){
	// Get list of all albums that belong to the user requesting this URL
	app.get('/albums', restrict, function(req, res){
		var userID = req.session.user._id;
		var getAlbums = require('../models/pictures').getAlbums;
		getAlbums(userID, function(err, albums){
			if (err) res.send('There was an error: ' + err);
			res.json(albums);
		})
	});
	// Get the album with specified ID
	app.get('/albums/:id', restrict, function(req, res){
		var albumID = req.params.id;
		var getAlbum = require('../models/pictures').getAlbum;
		// Getting current album data
		getAlbum(albumID, function(err, album){ 
			if (err) res.send('There was an error: ' + err);
			res.json(album);			
		});	
	});
	// Add the new album that will belong to the user posting to this URL
	app.post('/albums', restrict, function(req, res){
		var userID = req.session.user._id,
			albumTitle = req.body.title,
			albumDescription = req.body.desc,
			titleImage = '/img/default_album_title.jpg'

		var addAlbum = require('../models/pictures').addAlbum;

		addAlbum(userID, albumTitle, titleImage, albumDescription, function(err, newAlbum){
			if (err) res.send('There was an error: ' + err);
			console.log('Album: ' + albumTitle + ' >> was created successfully!');
			res.json(newAlbum);
		})
	});
	// Update the album with specified ID
	app.put('/albums/:id', restrict, isOwner, function(req, res){
		var albumID = req.params.id,
			newData = {
				title: req.body.title,
				desc: req.body.desc,
				title_image: req.body.title_image,
				shared: req.body.shared,
				shared_with: req.body.shared_with
			};
		var updateAlbum = require('../models/pictures').updateAlbum;
		updateAlbum(albumID, newData, function(err){
			if (err) res.send('There was an error: ' + err);
			console.log('Album with ID: ' + albumID + ' >> was updated.')
			res.send('Album was updated!')
		})
	})
	// Delete the album with specified ID
	app.delete('/albums/:id', restrict, isOwner, function(req, res){
		var albumID = req.params.id;
		var deleteAlbum = require('../models/pictures').deleteAlbum
		deleteAlbum(albumID, function(err){
			if (err) res.send('There was an error: ' + err);
		});
	});
	// Share the album
	app.post('/albums/share', restrict, isOwner, function(req, res){
		var albumID = req.body.aid;
		var users = req.body.users;
		var shareAlbum = require('../models/pictures').shareAlbum;
		shareAlbum(albumID, users, function(err){
			if (err) throw err;
			console.log('Shared successfully');
		})
		res.send('success!');
	});
	// Uploading pictures
	app.post('/albums/upload', restrict, function(req, res){
		var formidable 	= require('formidable'),
			path		= require('path'),
			addPicture	= require('../models/pictures').addPicture,
			im 			= require('imagemagick'),
			util		= require('util'),
			fs 			= require('fs');
		var mask = path.normalize(__dirname + "/../public"); // will exclude path till app folder
		var form = new formidable.IncomingForm();
		// Form settings:
		form.encoding = 'utf-8';
		form.uploadDir = __dirname + "/../public/uploads";
		form.keepExtensions = true;
		form.type = 'multipart';
		form.maxFieldsSize = 3 * 1024 * 1024;
		form.multiples = true;
		
		// Processing the form
		
		var aid 							// AlbimID, parsed separately, will be used when loading info to DB
		var pictures = [];					// parsed files will be cashed here in JSON format
		var addedPictures = []; 			// will be used to collect saved to DB pictures and return with ajax response

		form
			.on('field', function(field, value){
				aid = value
			})
			.on('file', function(field, file) {
				if (file.name.length == 0) return;// if form was empty - silently return
				pictures.push(file.toJSON())		
			})
			.on('end', function() {
				// Now when all files written to disk, let's make thumbs and DB records
				pictures.forEach(function(picture){

					// Small hack to keep original file name
					var newPath = path.normalize(form.uploadDir + "/" + picture.name);
					fs.renameSync(picture.path, newPath)

					// Creating and saving the thumb
					var thumbPath = path.normalize(form.uploadDir + '/thumb-' + picture.name)
					im.convert([newPath, '-resize', '200x200', thumbPath], function(){
						// Making record in DB
					addPicture({
						title: '', 									// passing title of picture, default is blank
						picturePath: newPath.replace(mask, ""), 	// passing path to image
						thumbPath: thumbPath.replace(mask, ""), 	// passing path to thumbnail
						userID: req.session.user._id, 				// passing owner id
						albumID: aid 								// passing album id
					},function(err, newPicture){					// callback function to execute when DB record is done
						if (err) console.log(err);
						addedPictures.push(newPicture);
						if (addedPictures.length == pictures.length) { // if it was the last picture, then send response
							res.send('success');
						}
					});
					});

					
				});
			});
		form.parse(req);
	});
	app.delete('/pictures/:id', restrict, isOwner, function(req, res){
		var pictureID = req.params.id;
		var deletePicture = require('../models/pictures').deletePicture;

		deletePicture(pictureID, function(err){
			if (err) res.send('There was an error: ' + err);
		})
	})
}