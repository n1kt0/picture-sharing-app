// ROUTES MIDDLEWARES
var restrict = require('./middlewares').restrict

// ROUTES
module.exports = function(app){
	
	app.get('/', restrict, function(req, res){
		var getAlbums = require('../models/pictures').getAlbums
		getAlbums(req.session.user._id, function(err, albums){ // albums object contains 2 sub-objects inside: 1) own albums, 2) shared ones
			if (err) throw err;
			res.locals = {
				title: "Pic Sharing App Dashboard",
				userName: req.session.user.name,
				userID: req.session.user._id,
				albums: JSON.stringify(albums)
				// ownAlbums: albums.own,
				// sharedAlbums: albums.shared
			}
			// Providing access to site admin
			
			if (req.session.user.role == 'admin') {
				res.locals.admin = true;
			}
			res.render('index')
		});
	});
	
	app.get('/login', function(req, res){
		res.locals = {
			title: "Pic Sharing App Dashboard"
		}
        console.log(res.engines)
        console.log(app.engines['.html'].toString())
		res.render('login')
	});
	
	app.post('/login', function(req, res){
		var authenticate = require('../models/user').authenticate
		authenticate(req.body.username, req.body.password, function(err, user){
			if (user) {
				req.session.regenerate(function(){
					req.session.user = { // Passing only public-friendly data, not the complete user object we received from auth func
						name: user.name,
						role: user.role,
						_id: user._id
					}
					res.redirect('/')
				})
			} else {
				req.session.error = 'Authentication failed, please check your things.'
				res.redirect('/login')
			}
		});
	});

	app.get('/logout', function(req, res){
		req.session.destroy(function(){
			res.redirect('/');
		});
	});
}