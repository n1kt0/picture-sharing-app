module.exports = {
	restrict: function(req, res, next){
		if (req.session.user) {
		    next();
		} else {
		    req.session.error = 'Access denied!';
		    console.log('IP: ' + req.ip + ' > tried to access: ' + req.path)
		    res.redirect('/login');
		}
	},
	isAdmin: function(req, res, next){
		var getUser = require('../models/user').getUser
		getUser({name: req.session.user.name}, function(err, user){
			if (user.role == 'admin') {
				next()
			} else {
			req.session.error = 'Admin rights required!';
			console.log('User: ' + req.session.user.name + ' with role: ' + user.role + ' > tried to access: ' + req.path)
			res.redirect('/')
			}
		});
	},
	isOwner: function(req, res, next){ // checks whether the user is the owner if the album 
		var user 		= req.session.user,
			objectID 	= req.body.aid || req.params.id,  // caching ID from AJAX req or from API req
			whatTocheck = req.url.split('/')[1] // determine which object should we check the owner of. Can be 'albums' or 'pictures'
		
		var checker = function(model, objectID){ // define a func to do an owner check operation
			model.findOne({_id: objectID}, 'owner', function(err, object){
				if (err || !object) {
					res.send('There was an error: ' + (err ? err : 'object with specified ID was not found.'));
					return
				}
				if (object.owner == user._id || user.role == 'admin') { // only owner or admin are allowed to proceed further
					next();
				} else {
					res.send('Access error: only owner can perform this action.')
				}
			});
		}

		if (whatTocheck == 'albums') {
			var	albums 	= require('../models/models').Album;
			checker(albums, objectID);
		} else {
			var	pictures = require('../models/models').Picture;
			checker(pictures, objectID);
		}

	}
}